
# ESP8266-Circular-Display-GC9A01

Copyright Kris Occhipinti 2024-02-02

(https://filmsbykris.com)

License GPLv3

Device - https://www.amazon.com/AITRIP-Display-Interface-240x240-Raspberry/dp/B0BL7G3HTP

Tested using ESP8266 LOLIN Wemos D1 Mini Lite

# Pins
- Note Original tutorial doesn't connect RST
- it would not work properly for me with out RST connection
```
RST	D4
CS	D8
DC	D2
SDA	D7
SCL	D5
GND	GND
VCC	3V3
```

# Links
- Based on Tutorial - https://thesolaruniverse.wordpress.com/2022/11/01/an-internet-synced-clock-circular-display-with-gc9a01-controller-powered-by-an-esp8266/
- Time Zones - https://www.epochconverter.com/timezones?q=1706885100&tz=America%2FNew_York

